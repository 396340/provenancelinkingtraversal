package bthesis.provenancechain;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.security.NoSuchAlgorithmException;

import bthesis.provenancechain.logic.Crawler;
import bthesis.provenancechain.simulation.SimMetaHashRetriever;
import bthesis.provenancechain.config.ConfigLoader;
import bthesis.provenancechain.config.Configuration;
import bthesis.provenancechain.tools.loading.LoaderResolver;
import bthesis.provenancechain.tools.retrieving.IMetaHashRetriever;

import org.openprovenance.prov.model.Document;
import org.openprovenance.prov.model.QualifiedName;
import org.openprovenance.prov.vanilla.ProvFactory;

import bthesis.provenancechain.tools.security.HashDocument;

/**
 * Main runtime class responsible for the execution of the application.
 *
 * @author Tomas Zobac
 */
public class MainRuntime {
    private static final Crawler crawler;
    private static final Map<String, QualifiedName> connectors;
    private static final IMetaHashRetriever metaHashRetriever;
    private static final LoaderResolver resolver;

    /*
      Initializes necessary components and loads configurations.
     */
    static {
        Configuration config = ConfigLoader.loadConfig();
        ProvFactory provFactory = new ProvFactory();
        connectors = new HashMap<>(){{
            put("mainActivity", provFactory.newQualifiedName(config.cpmUri, config.mainActivity, null));
            put("receiverConnector", provFactory.newQualifiedName(config.cpmUri, config.receiverConnector, null));
            put("senderConnector", provFactory.newQualifiedName(config.cpmUri, config.senderConnector, null));
            put("externalConnector", provFactory.newQualifiedName(config.cpmUri, config.externalConnector, null));
        }};

        metaHashRetriever = new SimMetaHashRetriever();
        resolver = new LoaderResolver();

        crawler = new Crawler(connectors, metaHashRetriever, resolver);
    }

    /**
     * Main method to run the application. It provides a command-line interface
     * to interact with various functionalities of the application.
     *
     * @throws IOException if there's an error in I/O operations.
     */
    public static void run() throws IOException {
       try {
            System.out.println("\nLOOKING FOR PRECURSORS!");

            findPrecursors();
        } catch (Exception e) {
            System.out.println("Cause: " + e.getCause());
            throw new RuntimeException(e);
        }
        System.out.println("Exiting the program.");
    }

    /**
     * Finds and prints the precursors of a given entity and bundle.
     *
     * * @throws NoSuchAlgorithmException if there's an error with the hashing algorithm.
     */
    private static void findPrecursors() throws NoSuchAlgorithmException {
        ProvFactory provFactory = new ProvFactory();

        //Digital pathology use case
        QualifiedName entity = provFactory.newQualifiedName("https://doi.org/10.58092/", "trainedModel", null);
        QualifiedName bundle = provFactory.newQualifiedName("https://gitlab.ics.muni.cz/422328/pid-test/-/blob/master/provn/", "eval.provn", null);

        //ColoRectal Cancer (CRC) cohort extension use case
        //QualifiedName entity = provFactory.newQualifiedName("https://doi.org/10.58092/", "CRCC_dataset_connector", null);
        //QualifiedName bundle = provFactory.newQualifiedName("https://repolab.crs4.it/dh/linking-paper-image-conversion-provenance/-/tree/main/output/research_institution/provn/", "CRCC_analysis.provn", null);

        if (entity == null || bundle == null) {
            System.out.println("All values must be specified");
            return;
        }

        Document document = resolver.load(bundle);
        crawler.getPrecursors(entity, document);

        crawler.getNodes().forEach(item ->
                System.out.println("Precursor ID: " + item.connector() + " present in bundle: \n" +
                        "   " + item.bundle() + "\n"));

        crawler.cleanup();
    }
}
