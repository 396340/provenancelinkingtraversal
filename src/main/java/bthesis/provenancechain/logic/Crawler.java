package bthesis.provenancechain.logic;

import java.io.ByteArrayInputStream;
import java.math.BigInteger;
import java.util.List;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

import bthesis.provenancechain.simulation.DoiPidResolver;
import bthesis.provenancechain.logic.data.ProvenanceNode;
import bthesis.provenancechain.tools.loading.GitLabFileLoader;
import bthesis.provenancechain.tools.loading.IFileLoader;
import bthesis.provenancechain.tools.retrieving.IMetaHashRetriever;
import bthesis.provenancechain.tools.metadata.IPidResolver;
import bthesis.provenancechain.tools.loading.LoaderResolver;
import org.openprovenance.prov.model.Bundle;
import org.openprovenance.prov.model.Entity;
import org.openprovenance.prov.model.Document;
import org.openprovenance.prov.model.Statement;
import org.openprovenance.prov.model.QualifiedName;
import org.openprovenance.prov.model.WasDerivedFrom;
import org.openprovenance.prov.interop.Formats;
import org.openprovenance.prov.interop.InteropFramework;

import bthesis.provenancechain.tools.security.HashDocument;

/**
 * Facilitates the traversal of provenance data by crawling through documents and
 * retrieving provenance entities and their relationships.
 *
 * @author Tomas Zobac
 */
public class Crawler {
    private final QualifiedName receiverConnector;
    private final QualifiedName senderConnector;
    private List<QualifiedName> done;
    private List<ProvenanceNode> nodes;
    private final LoaderResolver resolver;
    private final IMetaHashRetriever metaHashRetriever;

    /**
     * Constructs a new Crawler with the specified connectors, and meta hash retriever.
     *
     * @param connectors A map of connector identifiers to their qualified names.
     * @param metaHashRetriever The meta hash retriever for fetching hash values.
     */
    public Crawler(Map<String, QualifiedName> connectors, IMetaHashRetriever metaHashRetriever, LoaderResolver resolver) {
        this.done = new ArrayList<>();
        this.nodes = new ArrayList<>();
        this.receiverConnector = connectors.get("receiverConnector");
        this.senderConnector = connectors.get("senderConnector");
        this.resolver = resolver;
        this.metaHashRetriever = metaHashRetriever;
    }

    /**
     * Retrieves the list of provenance nodes found during the crawl.
     *
     * @return A list of ProvenanceNode instances.
     */
    public List<ProvenanceNode> getNodes() {
        return this.nodes;
    }

    /**
     * Resets the internal lists of processed nodes and found nodes.
     */
    public void cleanup() {
        this.done = new ArrayList<>();
        this.nodes = new ArrayList<>();
    }

    /**
     * Retrieves the precursors of a given entity and bundle.
     * <p>
     * Note:
     * - The 'done' list keeps track of already processed entities to avoid reprocessing.
     * - The 'nodes' list accumulates the precursor nodes found during the crawl.
     * <p>
     * The algorithm consists of three main if statements checking for the current entity's type:
     * 1. if the type is a sender connector, it will add the found precursor into the nodes list
     * 2. if the type is a receiver connector, the traversal reached the end of the document and the method is
     *    called again with new location parameters
     * 3. if the type is neither sender of receiver connector, the algorithm will loop over other statements
     *    in the document, until it finds one of a WasDerivedFrom type which it will use to find next entity
     *
     * @param entityId The ID of the entity to start the crawl.
     * @param document A document containing the entity.
     * @throws NoSuchAlgorithmException If the hashing algorithm is not available.
     */
    public void getPrecursors(QualifiedName entityId, Document document) throws NoSuchAlgorithmException {
        Bundle docBundle = (Bundle) document.getStatementOrBundle().get(0);
        QualifiedName entityType = getEntityType(entityId, document);

        if (entityType == null) throw new RuntimeException("entityType is null for: \n" + docBundle.getId() + "\n" + entityId);

        if (entityType.equals(this.senderConnector)) {
            //only forward connectors are considered as precursors, and for that reason, their ID is considered as part of the result
            this.nodes.add(new ProvenanceNode(entityId, docBundle.getId(), null));
            this.done.add(entityId);
        }
        if (entityType.equals(this.receiverConnector)) {
            this.done.add(entityId);

            //resolving PID to get information about the destination bundle which is linked by the connector
            IPidResolver pidResolver = new DoiPidResolver();
            Map<String, QualifiedName> connectorEntry = pidResolver.getConnectorEntry(entityId, this.receiverConnector);

            //retrieving referenced bundle ID and referenced metabundle ID from the PID-resolved information
            QualifiedName referencedBundleId = connectorEntry.get("referenceBundleID");
            QualifiedName metaDocQN = connectorEntry.get("metaID");

            //retrieving referenced bundle and its metabundle
            document = this.resolver.load(referencedBundleId);
            Document metaDocument = this.resolver.load(metaDocQN);

            //retrieving referenced bundle as byte array -- used to compute hash
            GitLabFileLoader loader = new GitLabFileLoader();
            ByteArrayInputStream documentAsBytesToHash = loader.loadFileAsBytes(referencedBundleId.getUri());

            //verifying the hash
            if (!checkSum(document, documentAsBytesToHash, metaDocument)) throw new RuntimeException("Checksum failed for: " + docBundle.getId() + "\n" + "\nTerminating traversal!");

            //if the hash was OK, continue traversing the referenced bundle
            getPrecursors(entityId, document);
        } else {
            //traversing the graph to get unprocessed connectors that has been used to derive the current entity
            for (Statement statement : docBundle.getStatement()) {
                //looking for wasDerivedFrom relation
                if (statement instanceof WasDerivedFrom derived)
                {
                    //checking whether the current entity is derived in the derivation
                    if ((derived.getGeneratedEntity().equals(entityId)))
                    {
                        //if yes, check whether the original entity is processed already
                        //if it was not processed yet, then process it
                        QualifiedName connector = derived.getUsedEntity();
                        if (!(this.done.contains(connector))) {
                            this.done.add(connector);
                            //if not processed yet, we need to find its precursors
                            getPrecursors(connector, document);
                        }
                    }
                }
            }
        }
    }

    /**
     * Retrieves a connector type of entity from the provided document.
     *
     * @param entityId The ID of the entity whose connector's type is to be retrieved.
     * @param document The document containing the entity.
     * @return The type of the entity as a QualifiedName, or null if the entity is not found.
     */
    private QualifiedName getEntityType(QualifiedName entityId, Document document) {
        Bundle bundle = (Bundle) document.getStatementOrBundle().get(0);
        for (Statement statement : bundle.getStatement()) {
            if (statement instanceof Entity entity) {
                if (entity.getId().equals(entityId)) {
                    return (QualifiedName) entity.getType().get(0).getValue();
                }
            }
        }
        return null;
    }

    /**
     * Verifies the integrity of the provided document by comparing its hash with the hash stored in the meta document.
     *
     * @param document The document whose integrity is to be verified.
     * @return A string indicating the result of the checksum verification.
     * @throws NoSuchAlgorithmException If the hashing algorithm is not available.
     */
    public boolean checkSum(Document document, ByteArrayInputStream documentAsBytesToHash, Document metaDocument) throws NoSuchAlgorithmException {
        //compute hash for given document
        HashDocument hasher = new HashDocument();
        String sha256 = hasher.generateSHA256(documentAsBytesToHash.readAllBytes());

        //retrieve the stored hash from the meta bundle
        Bundle docBundle = (Bundle) document.getStatementOrBundle().get(0);
        Map<String, String> hashes = metaHashRetriever.retrieveHash(metaDocument, docBundle.getId());

        //converting the hashes into big integer to get rid of zero prefixes
        BigInteger in1 = new BigInteger(hashes.get("sha256"), 16);
        BigInteger in2 = new BigInteger(sha256, 16);

        //compare the hashes
        return in1.equals(in2);
    }
}
