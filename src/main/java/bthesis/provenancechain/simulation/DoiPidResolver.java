package bthesis.provenancechain.simulation;

import bthesis.provenancechain.tools.loading.GitLabFileLoader;
import bthesis.provenancechain.tools.metadata.IPidResolver;
import org.glassfish.jersey.message.internal.Qualified;
import org.openprovenance.prov.model.Entity;
import org.openprovenance.prov.model.QualifiedName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.io.InputStream;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.w3c.dom.*;
import org.openprovenance.prov.model.*;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

import java.net.URL;

public class DoiPidResolver implements IPidResolver {

    /**
     * Default constructor for the LocalPidResolver.
     */
    public DoiPidResolver() {

    }

    @Override
    public Map<String, QualifiedName> getConnectorEntry(QualifiedName entityId, QualifiedName entityType) {

        try{
            String rawResolvedString = GetRawDataCiteResponse(entityId);
            String bundleId = getBundleIdFromDataCiteResponse(rawResolvedString);

            //System.out.println("Resolved PID Bundle id:" + bundleId);

            GitLabFileLoader loader = new GitLabFileLoader();
            org.openprovenance.prov.model.Document provDocument =  loader.loadFile(bundleId);

            List<Map<String, QualifiedName>> table = translateCPMmetaDocumentToTable(provDocument);

            //System.out.println("PRINTING RESULTING TABLE for resolved PID bundle ID");
            //for(Map<String, QualifiedName> row : table){
            //    System.out.println(row.toString());
            //}

            for (Map<String, QualifiedName> tableRow:table) {
                if(tableRow.get("entityID").equals(entityId) && tableRow.get("connectorID").equals(entityType))
                    return tableRow;
            }
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }

        return null;
    }

    private List<Map<String, QualifiedName>> translateCPMmetaDocumentToTable(org.openprovenance.prov.model.Document metaDocument){
        List<StatementOrBundle> statements = metaDocument.getStatementOrBundle();
        List<Map<String, QualifiedName>> resultingTable = new ArrayList<>();

        for (StatementOrBundle statement : statements)
        {
            Map<String, QualifiedName> tableRow = new HashMap<String, QualifiedName>();

            Entity entity = (Entity) statement;
            QualifiedName entityType = (QualifiedName) entity.getType().get(0).getValue();
            tableRow.put("entityID", entity.getId());
            tableRow.put("connectorID", entityType);

            QualifiedName qnSenderBundleIdAttribute = new org.openprovenance.prov.vanilla.QualifiedName("https://www.commonprovenancemodel.org/cpm-namespace-v1-0/", "senderBundleId", "cpm");
            QualifiedName qnReceiverBundleIdAttribute = new org.openprovenance.prov.vanilla.QualifiedName("https://www.commonprovenancemodel.org/cpm-namespace-v1-0/", "receiverBundleId", "cpm");
            QualifiedName qnCurrentBundleIdAttribute = new org.openprovenance.prov.vanilla.QualifiedName("https://www.commonprovenancemodel.org/cpm-namespace-v1-0/", "currentBundle", "cpm");
            QualifiedName qnmetabundle = new org.openprovenance.prov.vanilla.QualifiedName("https://www.commonprovenancemodel.org/cpm-namespace-v1-0/", "metabundle", "cpm");

            for (Other attribute : entity.getOther()){
                if(attribute.getElementName().equals(qnSenderBundleIdAttribute) || attribute.getElementName().equals(qnReceiverBundleIdAttribute) || attribute.getElementName().equals(qnCurrentBundleIdAttribute)){
                    QualifiedName q = (QualifiedName) attribute.getValue();
                    tableRow.put("referenceBundleID", q);
                }

                else if ( attribute.getElementName().equals(qnmetabundle)){
                    QualifiedName q = (QualifiedName) attribute.getValue();
                    tableRow.put("metaID", q);
                }
            }

            resultingTable.add(tableRow);
        }

        return resultingTable;
    }

    private String GetRawDataCiteResponse(QualifiedName entityId) throws Exception{
        //String command ="curl -X GET https://doi.org/10.58092/WSIDataConnectorPreproc";
        String command2 ="curl -X GET " + entityId.getUri();
        ProcessBuilder processBuilder = new ProcessBuilder(command2.split(" "));
        InputStream inputStream = processBuilder.start().getInputStream();;
        return new String(inputStream.readAllBytes());
    }

    private String getBundleIdFromDataCiteResponse(String rawResolvedString) throws Exception{
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = dbf.newDocumentBuilder();
        org.w3c.dom.Document doc = builder.parse(new InputSource(new StringReader(rawResolvedString)));
        doc.getDocumentElement().normalize();

        NodeList nodeList = doc.getDocumentElement().getChildNodes();
        String bundleId = nodeList.item(2).getFirstChild().getFirstChild().getNodeValue();

        return bundleId;
    }
}
