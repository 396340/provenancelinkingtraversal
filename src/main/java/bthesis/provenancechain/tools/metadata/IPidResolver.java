package bthesis.provenancechain.tools.metadata;

import org.openprovenance.prov.model.QualifiedName;

import java.util.List;
import java.util.Map;

/**
 * Represents an interface for resolving PIDs (Persistent Identifiers) and working with the navigational table.
 *
 * @author Tomas Zobac
 */
public interface IPidResolver {
    /**
     * Resolves a given entity ID and entity type to a row from the navigational table.
     *
     * @param entityId The ID of the entity to be resolved.
     * @param entityType The type of the entity.
     * @return A map containing key-value pairs related to the resolved entity.
     */
    Map<String, QualifiedName> getConnectorEntry(QualifiedName entityId, QualifiedName entityType);

}
