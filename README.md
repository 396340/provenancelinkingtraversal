# Provenance chain backbone traversal utility 
#### The code is based on the bachelor thesis of Tomáš Zobač
#### The code was revised by Rudolf Wittner

The repository contains code to validate the mechanism for linking provenance and meta-provenance components in the Common Provenance Model. 

## Building
The implementation uses a Maven Shade plugin for jar packaging

1. Open the cloned repo in the console
2. Run `mvn clean package`
3. Execute the created jar with `java -jar .\target\BThesis-ProvenanceChain-VERSION-shaded.jar`
    - alternatively, if the targeted environment doesn't have a JRE, you can create an exe installer
   using Java's prepackaged `jpackage` command line tool

## Running
The code can be run on two provenance chains:
1) Provenance chain documenting a digital pathology use case.
2) Provenance chain documenting a ColoRectal Cancer (CRC) cohort extension use case.

To run the algorithm on a selected use case, comment/uncomment the lines 78-83 in the MainRuntime file accordingly. 

